require "test_helper"

class DetailOrderControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get detail_order_index_url
    assert_response :success
  end

  test "should get create" do
    get detail_order_create_url
    assert_response :success
  end

  test "should get show" do
    get detail_order_show_url
    assert_response :success
  end

  test "should get update" do
    get detail_order_update_url
    assert_response :success
  end

  test "should get destroy" do
    get detail_order_destroy_url
    assert_response :success
  end

  test "should get detail_order_params" do
    get detail_order_detail_order_params_url
    assert_response :success
  end
end
