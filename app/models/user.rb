class User < ApplicationRecord
    has_secure_password
    has_many :orders, dependent: :destroy

    def user_data
        {
          id: self.id,
          name: self.name,
          username: self.username,
          password_digest: self.password_digest,
          role: self.role,
          created_at: self.created_at,
          updated_at: self.updated_at,
        }
      end
    
end
