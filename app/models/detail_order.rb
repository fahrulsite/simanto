class DetailOrder < ApplicationRecord
    belongs_to :order
    belongs_to :products 

    def detail_data
        {
          id: self.id,
          order_id: self.order_id,
          product_id: self.product_id,
          qty: self.qty,
          price: self.price,
          created_at: self.created_at,
          updated_at: self.updated_at,
        }
      end
end
