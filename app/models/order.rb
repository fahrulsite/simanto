class Order < ApplicationRecord
    belongs_to :user
    has_many :detail_orders

    def order_data
        {
          id: self.id,
          user_id: self.user_id,
          total: self.total,
          created_at: self.created_at,
          updated_at: self.updated_at,
        }
    end

    def order_data_detailed
        {
          id: self.id,
          user_id: self.user_id,
          total: self.total,
          created_at: self.created_at,
          updated_at: self.updated_at,
          list_data: self.detail_orders

        }
    end
end
