class Product < ApplicationRecord
    has_many :detail_orders
    belongs_to :category 

    def product_data
        {
          id: self.id,
          name: self.name,
          des: self.desc,
          stock: self.stock,
          category_id: self.category_id,
          price: self.price,
        }
      end
end
