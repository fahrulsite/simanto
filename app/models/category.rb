class Category < ApplicationRecord
    has_many :products
    
    def data
        {
            id: self.id,
            name: self.name,
            desc: self.desc
        }
    end
end
