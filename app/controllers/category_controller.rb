class CategoryController < ApplicationController
  before_action :set_category, only: [:show, :update, :destroy]
    def index
        @category = Category.all
        render json: @category
    end

    def create
        @category = Category.new(category_params)
        if @category.save
          render json: @category
        else
          render error: {error:'Unable to create Category Order'}, status:404
        end
    end
    
    def show
      render json: @category.data
    end
    
      def update
        @category = Category.find(params[:id])
        if @category.update(category_params)
          render json: @category.data
        else
          render json: @category.errors, status: :unprocessable_entity
        end
      end
    
      def destroy
        @category = Category.find(params[:id])
        if @category
          @category.destroy
          render json:{messages: 'Category successfully deleted'}, status: 200
        else
          render error:{error:'Unable to delete Category.'}, status:400
        end 
      end
    
      private
      def set_category
        @category = Category.find_by_id(params[:id])
        if @category.nil?
          render json: { error: "Category not found" }, status: :not_found
        end
      end
      
      def category_params
        params.require(:category).permit(:name,:desc)
      end
end