class ProductController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]
  
  def index
    @product = Product.all
    render json: @product
  end

  def create
    @product = Product.new(product_params)
    if @product.save
        render json: @product
    else
        render error: {error:'Unable to create Product.'}, status:400
    end
  end

  def show
    render json: @product.product_data
  end

  def update
    @product = Product.find(params[:id])
    if @product.update(product_params)
        render json: {messages:'Product successfully updated' }, status:200
    else
        render error: {error:'Unable to update Product.'}, status:400
    end
  end

  def destroy
    @product = Product.find(params[:id])
    if @product
        @product.destroy
        render json:{messages:'Product successfully deleted' }, status:200
    else
        render error:{error:'Unable to delete Product.'}, status:400
    end
  end
  
  def set_product
    @product = Product.find_by_id(params[:id])
    if @product.nil?
      render json: { error: "Product not found" }, status: :not_found
    end
  end

  def product_params
    params.require(:product).permit(:name, :desc, :stock, :category_id, :price)
  end
end
