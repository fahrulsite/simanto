class DetailOrderController < ApplicationController
  before_action :set_detail_order, only: [:show, :update, :destroy]
  
  def index
    @detail_order = DetailOrder.all
    render json: @detail_order
  end

  def create
    @detail_order = DetailOrder.new(detail_order_params)
    if @detail_order.save
      render json: @detail_order
    end
      render error: {error:'Unable to create Detail Order'}, status:404
  end

  def show
    render json: @detail_order.detail_data
  end

  def update
    @detail_order = DetailOrder.find(params[:id])
    if @detail_order.update(detail_order_params)
      render json: @detail_order.detail_data
    else
      render json: @detail_order.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @detail_order = DetailOrder.find(params[:id])
    if @detail_order
      @detail_order.destroy
      render json:{messages: 'Detail successfully deleted'}, status: 200
    else
      render error:{error:'Unable to delete Detail.'}, status:400
    end 
  end

  private
  def set_detail_order
    @detail_order = DetailOrder.find_by_id(params[:id])
    if @detail_order.nil?
      render json: { error: "Detail not found" }, status: :not_found
    end
  end
  
  def detail_order_params
    params.require(:detail_order).permit(:order_id,:product_id, :qty, :total)
  end
end
