class OrderController < ApplicationController
    before_action :set_order, only: [:show, :update, :destroy]

    def index
        @order = Order.all
        render json: @order
      end
    
      def create
        @order = Order.new(order_params)
        if @order.save
          render json: @order
        else
          render error: {error:'Unable to create Order'}, status:404
        end
      end
    
      def show
        render json: @order.order_data_detailed
      end
    
      def update
        @order = Order.find(params[:id])
        if @order.update(order_params)
          render json: @order.order_data
        else
          render json: @order.errors, status: :unprocessable_entity
        end
      end
    
      def destroy
        @order = Order.find(params[:id])
        if @order
          @order.destroy
          render json:{messages: 'Detail successfully deleted'}, status: 200
        else
          render error:{error:'Unable to delete Order.'}, status:400
        end 
      end
    
      def set_order
        @order = Order.find_by_id(params[:id])
        if @order.nil?
          render json: { error: "Order not found" }, status: :not_found
        end
      end
      
      def order_params
        # {
        #   user_id: params[:user_id],
        #   total: params[:total],
        # }

        params.require(:order).permit(:user_id, :total)
      end
end
