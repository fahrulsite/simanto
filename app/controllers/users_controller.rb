class UsersController < ApplicationController
  skip_before_action :authenticate_request, only: [:create, :login]
    before_action :set_user, only: [:show, :update, :destroy]
  
 def index
    @users = User.all
    render json: @users
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user.user_data, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def show
    render json: @user.user_data
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
        # render json: {messages:'User successfully updated' }, status:200
        render json: @user.user_data
    else
        # render error: {error:'Unable to update User.'}, status:400
        render json: @user.errors, status: :unprocessable_entity
    end
  end

  def login
    @user = User.find_by(username: params[:username])
    if @user && @user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      render json: {
        user: @user.user_data,
        token: token,
      }
    else
      render json: { error: "Invalid username or password" }, status: :unauthorized
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user
        @user.destroy
        render json:{messages:'User successfully deleted' }, status:200
    else
        render error:{error:'Unable to delete User.'}, status:400
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find_by_id(params[:id])
    if @user.nil?
      render json: { error: "User not found" }, status: :not_found
    end
  end

  def user_params
    # params.require(:user).permit(:name, :username, :password, :role)
    {
      name:params[:name],
      username:params[:username],
      password:params[:password],
      role:params[:role],
    }
  end

  

end
