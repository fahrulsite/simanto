class CreateDetailOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :detail_orders do |t|
      t.integer :order_id
      t.integer :product_id
      t.integer :qty
      t.integer :price

      t.timestamps
    end
  end
end
