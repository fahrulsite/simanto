class CreateProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :desc
      t.integer :stock
      t.integer :category_id
      t.integer :price

      t.timestamps
    end
  end
end
